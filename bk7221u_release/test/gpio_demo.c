/*
* 程序清单： 这是一个PIN 设备使用例程
* 例程导出了pin_led_sample 命令到控制终端
* 命令调用格式： pin_led_sample
* 程序功能： 通过按键控制led 对应引脚的电平状态控制led
*/
#include <rtthread.h>
#include <rtdevice.h>

#define LED_PIN_NUM 30
#define LED1_PIN_NUM 13
#define KEY0_LED2_PIN_NUM 12  //27
#define KEY0_PIN_NUM 2
#define KEY1_PIN_NUM 3
#define KEYUP_PIN_NUM 28
#define KEYDOWN_PIN_NUM 19
#define KEYDOOR_PIN_NUM 0
#define INFRARED_PIN_NUM 1//对应开发板上的SCL引脚
int DoorControl_in(void);
extern void mq_publish(const char *send_str);
void led_on(void *args) 
{
	rt_kprintf("KEY!\n");
	
	/* led 引脚为输出模式*/
	////rt_pin_mode(KEY0_LED2_PIN_NUM, PIN_MODE_OUTPUT);
	/* 默认低电平*/
	//rt_pin_write(KEY0_LED2_PIN_NUM, PIN_LOW);
	mq_publish("{\"dooropen\":1,\"photo\":1}\n");//mqtt发送到腾讯云平台,然后node-red的mqtt读取信息
    rt_kprintf("{\"dooropen\":1,\"photo\":1}\n");//串口发送到Node-red
}

void led_off(void *args) 
{
	rt_kprintf("turn off led2!\n");
	
	/* led 引脚为输出模式*/
	//rt_pin_mode(KEY0_LED2_PIN_NUM, PIN_MODE_OUTPUT);
	/* 默认低电平*/
	//rt_pin_write(KEY0_LED2_PIN_NUM, PIN_HIGH);
	mq_publish("{\"dooropen\":0,\"photo\":0}\n");//mqtt发送到腾讯云平台,然后node-red的mqtt读取信息
    rt_kprintf("{\"dooropen\":0,\"photo\":0}\n");//串口发送到Node-red

}


static void pin_led_sample(void) 
{
	/* led 引脚为输出模式*/
	//rt_pin_mode(LED2_PIN_NUM, PIN_MODE_OUTPUT);
	/* 默认低电平*/
	//rt_pin_write(LED2_PIN_NUM, PIN_LOW);
	/* 按键up引脚为输入模式*/
	rt_pin_mode(KEYUP_PIN_NUM , PIN_MODE_INPUT_PULLUP);
	/* 绑定中断， 下降沿模式， 回调函数名为beep_on */
	rt_pin_attach_irq(KEYUP_PIN_NUM , PIN_IRQ_MODE_FALLING , led_on, RT_NULL);
	/* 使能中断*/
	rt_pin_irq_enable(KEYUP_PIN_NUM , PIN_IRQ_ENABLE);
	/* 按键down引脚为输入模式*/
	rt_pin_mode(KEYDOWN_PIN_NUM , PIN_MODE_INPUT_PULLUP);
	/* 绑定中断， 下降沿模式， 回调函数名为led_off */
	rt_pin_attach_irq(KEYDOWN_PIN_NUM , PIN_IRQ_MODE_FALLING , led_off, RT_NULL);
	/* 使能中断*/
	rt_pin_irq_enable(KEYDOWN_PIN_NUM , PIN_IRQ_ENABLE);

    /* 按键center引脚为输入模式*/
	rt_pin_mode(KEY0_LED2_PIN_NUM , PIN_MODE_INPUT_PULLUP);
	/* 绑定中断， 下降沿模式， 回调函数名为led_off */
	rt_pin_attach_irq(KEY0_LED2_PIN_NUM , PIN_IRQ_MODE_FALLING , led_on, RT_NULL);
	/* 使能中断*/
	rt_pin_irq_enable(KEY0_LED2_PIN_NUM , PIN_IRQ_ENABLE);


	/* 门按键开关引脚为输入模式*/
//	rt_pin_mode(KEYDOOR_PIN_NUM , PIN_MODE_INPUT_PULLUP);
	/* 绑定中断， 下降沿模式， 回调函数名为beep_on */
//	rt_pin_attach_irq(KEYDOOR_PIN_NUM , PIN_IRQ_MODE_FALLING , DoorControl_in, RT_NULL);
	/* 使能中断*/
//	rt_pin_irq_enable(KEYDOOR_PIN_NUM , PIN_IRQ_ENABLE);

	/* led 引脚为输出模式
	rt_pin_mode(LED1_PIN_NUM, PIN_MODE_OUTPUT);
	/* 默认低电平
	rt_pin_write(LED1_PIN_NUM, PIN_LOW);
	/* led 引脚为输出模式
	rt_pin_mode(LED2_PIN_NUM, PIN_MODE_OUTPUT);
	/* 默认低电平
	rt_pin_write(LED2_PIN_NUM, PIN_HIGH);*/
}
void led_run_task()
{
	//int sum=0;
  while(1)
	{
		//sum++;
	//rt_kprintf("LED task running times:%d!\n",sum);
	/* led 引脚为输出模式*/
	rt_pin_mode(LED1_PIN_NUM, PIN_MODE_OUTPUT);
	/* 默认低电平*/
	rt_pin_write(LED1_PIN_NUM, PIN_LOW);
	
	rt_thread_delay(500);
	/* led 引脚为输出模式*/
	rt_pin_mode(LED1_PIN_NUM, PIN_MODE_OUTPUT);
	/* 默认低电平*/
	rt_pin_write(LED1_PIN_NUM, PIN_HIGH);
	
	rt_thread_delay(500);

	}

}
int DoorControl_in(void)
{
  mq_publish("{\"dooropen\":1,\"photo\":1}\n");//mqtt发送到腾讯云平台,然后node-red的mqtt读取信息
  rt_kprintf("{\"dooropen\":1,\"photo\":1}\n");//串口发送到Node-red
  led_on(NULL);//LED2
  rt_thread_delay(400);
  led_off(NULL);//LED2
  rt_thread_delay(400); 
  return 0;
}
int DoorControl_out(void)
{
  mq_publish("{\"dooropen\":0,\"photo\":0}\n");//mqtt发送到腾讯云平台,然后node-red的mqtt读取信息
  rt_kprintf("{\"dooropen\":0,\"photo\":0}\n");//串口发送到Node-red
  led_on(NULL);//LED2
  rt_thread_delay(400);
  led_off(NULL);//LED2
  rt_thread_delay(400);
 
  return 0;
}
void scan_come(void)
{
		/* 门按键开关引脚为输入模式*/
	rt_pin_mode(KEYDOOR_PIN_NUM , PIN_MODE_INPUT_PULLUP);
	
	//rt_kprintf("启动门禁开关监测！");//串口发送到Node-red
	while(1)
	{
		if(rt_pin_read(KEYDOOR_PIN_NUM)==PIN_LOW)
		{
			rt_thread_delay(50);
			if(rt_pin_read(KEYDOOR_PIN_NUM)==PIN_LOW)
			{
			DoorControl_in();
			}
			
		}
		 rt_thread_delay(10);
	}

}

void led_task(void)
{
    rt_thread_t tid1;
	 
    tid1 = rt_thread_create("led_run_task", led_run_task, RT_NULL, 256*1, 13, 10);
	 
    rt_thread_startup(tid1);
	 
}
void who_task(void)
{
    
	rt_thread_t tid2;
    
	tid2 = rt_thread_create("scan_come", scan_come, RT_NULL, 256*1, 13, 10);
     
	rt_thread_startup(tid2);
}
/* 导出到msh 命令列表中*/
MSH_CMD_EXPORT(pin_led_sample , pin led sample);
MSH_CMD_EXPORT(led_off , pin led sample);
MSH_CMD_EXPORT(led_on, pin led sample);
MSH_CMD_EXPORT(led_task, pin led sample);
MSH_CMD_EXPORT(who_task, pin led sample);
